public interface IKillable
{
    void Kill();
}

public interface IDamage
{
    void SetDamage(int dmg);
}

