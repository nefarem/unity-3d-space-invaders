using UnityEngine;
using UnityEngine.UI;

public class Boss : MonoBehaviour, IKillable, IDamage
{
    public int hp;
    
    [SerializeField] Scrollbar HPBar;  // Pasek z HP bossa
    [SerializeField] GameObject laserPrefab; // Prefab lasera
    [SerializeField] GameObject specialLaserPrefab; // Prefab specjalnego lasera tylko dla bossa
    [SerializeField] GameObject firePrefab; // Prefab ognia
    [SerializeField] Transform normalLaserSpawn;  // Spawn normalnego lasera
    [SerializeField] Transform[] specialLasersSpawn; // Spawn specjalnych laserów
    [SerializeField] Transform[] damageSpawn; // Miejsce gdzie pojawi się ogień po trafieniu przez gracza

    enum MoveDirection {left, right}
    MoveDirection moveDirectionType;   

    float specialAttacktimer = 0f;
    float normalAttackTimer = 0f;
    float laserCooldownTime = 1f;
    float specialLaserCooldownTime = 8f;

    bool isOnCorrectPosition;

    GameManager gameManager;
    CreateVFX createVFX;

    int fireIndex; // Jest 7 slotów na efekt ognia na bossie


    /*
        Opis mechaniki (jest prosta):
        - Co 1 sekunde oddaje strzał zwykkłym laserem
        - Co 8 sekund oddaje strzał specjalnym laserem który "rozciąga się" od bossa aż do poziomu Y na którym porusza się gracz, odejmuje on 3 hp, znika po 6 sekundach
    */

    void Start() 
    {
        createVFX = GameObject.Find("GameManager").GetComponent<CreateVFX>();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    void Update() 
    {
        // Poruszanie się w stronę gracza na swoje pozycje na planszy
        if(transform.position.y > 2f) MoveToPosition();
        else isOnCorrectPosition = true;

        // Ruch w lewo i prawo od końca do konca mapy
        if(isOnCorrectPosition) VerticalMove();

        if(normalAttackTimer <= laserCooldownTime) normalAttackTimer += Time.deltaTime;
        if(specialAttacktimer <= specialLaserCooldownTime) specialAttacktimer += Time.deltaTime;

        if (normalAttackTimer >= laserCooldownTime) 
        {
            normalAttackTimer = 0f;
            Shoot();
        }

        if (specialAttacktimer >= specialLaserCooldownTime)
        {
            specialAttacktimer = 0f;

            Shoot();
            SpecialAttack();
        }
    }

    public void SetDamage(int dmg)
    {
        if (fireIndex <= 6)
        {
            GameObject fire = Instantiate(firePrefab, damageSpawn[fireIndex].position, Quaternion.identity);
            fire.transform.parent = gameObject.transform;

            fireIndex++;
        }

        hp--;

        float hpBar = hp/10f;

        HPBar.size = hpBar;
    }

    public void Kill()
    {
        gameManager.CompleteGame();
        Destroy(gameObject);
    }

    void Shoot()
    {
        Instantiate(laserPrefab, normalLaserSpawn.position, Quaternion.identity);
    }

    void SpecialAttack()
    {
        Instantiate(specialLaserPrefab, specialLasersSpawn[0].position, Quaternion.Euler(0, 0, 90));
        Instantiate(specialLaserPrefab, specialLasersSpawn[1].position, Quaternion.Euler(0, 0, 90));
    }

    // Poruszanie się w stronę gracza na swoje pozycje na planszy
    void MoveToPosition()
    {
        transform.position = new Vector2(transform.position.x, transform.position.y - 0.01f);
    }

    // lewo/prawo
    void VerticalMove()
    {
        if (gameObject.transform.position.x >= 6f) moveDirectionType = MoveDirection.left;
        if (gameObject.transform.position.x <= -6f) moveDirectionType = MoveDirection.right;

        switch (moveDirectionType )
        {
            case MoveDirection.left:
                transform.position = new Vector2(transform.position.x - 0.001f, transform.position.y);
            break;

            case MoveDirection.right:
                transform.position = new Vector2(transform.position.x + 0.001f, transform.position.y);
            break;
        }
    }    
}
