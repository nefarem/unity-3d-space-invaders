using UnityEngine;

public class Enemy : MonoBehaviour, IKillable, IDamage
{
    public int hp; // Liczba HP przeciwnika
    [HideInInspector] public int positionX;  // Pozycja X w tablicy LevelManager.enemyTab
    [HideInInspector] public int positionY; // Pozycja Y w tablicy LevelManager.enemyTab

    [Header("Laser settings")]
    [SerializeField] Transform laserSpawn; // Miejsce spawnu lasera
    [SerializeField] GameObject laserPrefab; // Prefab lasera

    [Header("")]
    [SerializeField] GameObject firePrefab; // Prefab ognia ukazującego że statek przeciwnika jest uszkodzony

    // Opóźnienie dla strzału
    float timer; 
    float cooldownTime;

    // Jest true gdy przeciwnik dotrze z góry po za mapą na swoją pozycję
    bool isOnCorrectPosition;

    GameObject leftShip; // Najdalszy statek po lewej stronie
    GameObject rightShip; // Najdalszy statek po prawej stronie

    enum MoveDirection {left, right}
    MoveDirection moveDirectionType;

    LevelManager levelManager;
    EnemyDrop enemyDrop;
    CreateVFX createVFX;

    void Start()
    {
        timer = 0f + Random.Range(0, 2f);
        cooldownTime = 5f;
        
        enemyDrop = GetComponent<EnemyDrop>();
        levelManager = GameObject.Find("GameManager").GetComponent<LevelManager>();
        createVFX = GameObject.Find("GameManager").GetComponent<CreateVFX>();

        leftShip = GetLeftFirstEnemyShip();
        rightShip = GetRightFirstEnemyShip();

        moveDirectionType = MoveDirection.right;
    }

    void Update()
    {     
        // Wyszukania nowych statków z lewej i prawej strony
        leftShip = GetLeftFirstEnemyShip();
        rightShip = GetRightFirstEnemyShip();

        if (timer <= cooldownTime && isOnCorrectPosition) timer += Time.deltaTime;

        // Określa czy przeciwnik może strzelać w stronę gracza czy nie. Możliwe gdy 'przed' obiektem przeciwnika nie ma innego oraz minął cooldown od ostatniego ataku
        if (timer >= cooldownTime && !HaveEnemyInFront()) Shoot();

        // Poruszanie się w stronę gracza na swoje pozycje na planszy
        if(transform.position.y > 2f + positionY) MoveToPosition();
        else isOnCorrectPosition = true;

        // Ruch w lewo i prawo od końca do konca mapy
        if(isOnCorrectPosition && rightShip != null && leftShip != null) VerticalMove();
    }


    // Metoda z interfejsu Ikillable, będzie wywoływana w skrypcie lasera gdy ten dotknie przeciwnika i ten ma 1 HP.
    public void Kill()
    {
        levelManager.enemyTab[positionX, positionY] = null;
        levelManager.enemyCounter--;

        // Zrespienie dropu jeżeli przeciw posiada go
        if (enemyDrop.hasDrop) Instantiate(enemyDrop.bonusPrefabsTab[Random.Range(0, enemyDrop.bonusPrefabsTab.Length)], gameObject.transform.position, Quaternion.identity);

        createVFX.SpawnVFX(gameObject.transform.position);

        Destroy(gameObject);
    }

    // Wywoływane w przypadku gdy przeciwnik ma 2 lub więcej HP.
    public void SetDamage(int dmg)
    {
        hp = hp - dmg;

        GameObject fire = Instantiate(firePrefab, new Vector2(gameObject.transform.position.x, gameObject.transform.position.y + 0.5f), Quaternion.identity);
        fire.transform.parent = gameObject.transform;
    }

    void Shoot()
    {
        timer = 0f;

        Instantiate(laserPrefab, laserSpawn.position, Quaternion.identity);
    }

    // Poruszanie się w stronę gracza na swoje pozycje na planszy
    void MoveToPosition()
    {
        transform.position = new Vector2(transform.position.x, transform.position.y - 0.01f);
    }

    void VerticalMove()
    {
        if (rightShip.transform.position.x >= 7f) moveDirectionType = MoveDirection.left;
        if (leftShip.transform.position.x <= -7f) moveDirectionType = MoveDirection.right;

        switch (moveDirectionType )
        {
            case MoveDirection.left:
                transform.position = new Vector2(transform.position.x - 0.001f, transform.position.y);
            break;

            case MoveDirection.right:
                transform.position = new Vector2(transform.position.x + 0.001f, transform.position.y);
            break;
        }
    }

    // Funkcja zwraca prawdę jeżeli ma innego przeciwnika przed sobą który zasłania mu ostrzał w stronę gracza
    bool HaveEnemyInFront()
    {
        if (positionY == 0) return false;
        else if(levelManager.enemyTab[positionX, positionY - 1] != null) return true;

        return false;
    }

    // Funkcja zwraca pierwsze od lewej statek wroga który nie został zniszczony który będzie stanowił "blokadę" dalszego ruchu w lewo gdy zblizy sie do krawędzi mapy
    GameObject GetLeftFirstEnemyShip()
    {
        int minX = 9999;
        int posY = 9999;

        foreach (GameObject item in levelManager.enemyTab)
        {
            if (item != null)
            {
               if (minX > item.GetComponent<Enemy>().positionX)
                {
                    minX = item.GetComponent<Enemy>().positionX;
                    posY = item.GetComponent<Enemy>().positionY;
                }              
            }
        }

        return levelManager.enemyTab[minX, posY];
    }

    // Działanie podobne do funkcji wyżej tylko od prawej strony
    GameObject GetRightFirstEnemyShip()
    {
        int maxX = 0;
        int posY = 0;

        int tabLineMaxX = levelManager.enemyCountInLine - 1;
        int tabLineMaxY = levelManager.currentWave - 1;

        for (int x = tabLineMaxX; x > 0; x--)
        {
            for (int y = tabLineMaxY; y > 0; y--)
            {
                if (levelManager.enemyTab[x, y] != null && maxX < tabLineMaxX)
                {
                    maxX = levelManager.enemyTab[x, y].GetComponent<Enemy>().positionX;
                    posY = levelManager.enemyTab[x, y].GetComponent<Enemy>().positionY;

                    return levelManager.enemyTab[maxX, posY];
                }
            }
        }

        foreach (GameObject item in levelManager.enemyTab)
        {
            if (levelManager.enemyTab.Length > 0 && item != null)
            {
                if (maxX < item.GetComponent<Enemy>().positionX)
                {
                    maxX = item.GetComponent<Enemy>().positionX;
                    posY = item.GetComponent<Enemy>().positionY;
                } 
            }
        }

        return levelManager.enemyTab[maxX, posY];
    }    
}
