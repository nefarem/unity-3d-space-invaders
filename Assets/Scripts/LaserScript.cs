using UnityEngine;

public class LaserScript : MonoBehaviour
{
    public Laser laser;
    SpriteRenderer spriteRenderer;

    GameManager gameManager;

    float speed = 20f;

    float time = 0f;

    void Awake()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        spriteRenderer = GetComponent<SpriteRenderer>();

        gameObject.name = laser.laserName;
        spriteRenderer.sprite = laser.laserSprite;
    }

    void Update() 
    {
        // Gdy pocisk wyleci po za kamerę to zostanie usunięty z gry
        if (transform.position.y >= 6f || transform.position.y <= -6f) Destroy(gameObject);

        if (!gameObject.CompareTag("BossLaser"))
        {
            switch (laser.laserTarget)
            {
                case Laser.Target.Player:
                    Move(-0.3f);
                break;

                case Laser.Target.Enemy:
                    Move(0.3f);
                break;
            }           
        }
        // Laser bossa
        else
        {
            time += Time.deltaTime;
            if (time >= 6f) Destroy(gameObject);
 
            if (gameObject.transform.localScale.x <= 3f)
            {
                gameObject.transform.localScale = new Vector2(gameObject.transform.localScale.x + 0.01f, gameObject.transform.localScale.y);
                gameObject.transform.position = new Vector2(gameObject.transform.position.x, gameObject.transform.position.y - 0.01f);              
            }

        }

    }

    void Move(float yPosMove)
    {
        transform.position = new Vector2(transform.position.x, transform.position.y + yPosMove * speed * Time.deltaTime);
    }

    void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            Enemy enemy = other.gameObject.GetComponent<Enemy>();

            if (enemy.hp == 1) enemy.Kill();
            else enemy.SetDamage(laser.damage);
            
            Destroy(gameObject);

            gameManager.score++;
        }

        if (other.gameObject.CompareTag("Boss") && laser.laserTarget == Laser.Target.Enemy)
        {
            Boss boss = other.gameObject.GetComponent<Boss>();

            if (boss.hp == 1) boss.Kill();
            else boss.SetDamage(laser.damage);
            
            Destroy(gameObject);
        }

        if (other.gameObject.CompareTag("Shield") && laser.laserTarget == Laser.Target.Player)
        {
            Shield shield = other.gameObject.GetComponent<Shield>();

            if (shield.hp == 0) shield.Kill();
            else shield.SetDamage(laser.damage);
            
            Destroy(gameObject);
        } 

        if (other.gameObject.CompareTag("Player"))
        {
            Player player = other.gameObject.GetComponent<Player>();

            player.SetDamage(laser.damage);

            Destroy(gameObject);
        }

        
    }
}
