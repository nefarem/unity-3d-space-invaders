using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public int score;
    public int highscore;

    UIManager uIManager;

    [HideInInspector] public Player player;

     void Awake() 
    {
        uIManager = GetComponent<UIManager>();
        player = GameObject.Find("Player").GetComponent<Player>();

        LevelsData.levelIndex = SceneManager.GetActiveScene().buildIndex;

        if (LevelsData.levelIndex == 1) player.HP = 2;
        else
        {
            // Wczytywanie danych z poprzedniego poziomu: NazwaZmiennej+indexPoziomu - 1 (bo insteresuje poprzedni), Wartość
            int previousLevel = LevelsData.levelIndex - 1;

            score = PlayerPrefs.GetInt("Score"+ previousLevel);
            highscore = PlayerPrefs.GetInt("Highscore"+ previousLevel);
            player.HP = PlayerPrefs.GetInt("PlayerHP"+ previousLevel);
        }
    }

    void Update()
    {
        if (player.HP < 0) player.HP = 0;
        if (score > highscore) highscore = score;

        if (player.HP == 0) GameOver(false);
    }

    public void GameOver(bool win)
    {
        if (win)
        {
            // Zapis danych po każdym poziomie: NazwaZmiennej+indexPoziomu, Wartość
            PlayerPrefs.SetInt("Score" + LevelsData.levelIndex, score);
            PlayerPrefs.SetInt("Highscore" + LevelsData.levelIndex, highscore);
            PlayerPrefs.SetInt("PlayerHP" + LevelsData.levelIndex, player.HP);
            PlayerPrefs.SetInt("CompleteLevel"+LevelsData.levelIndex, 1);

            uIManager.nextLevelPanel.SetActive(true);
        } 
        else uIManager.gameOverPanel.SetActive(true);
    }

    public void CompleteGame()
    {
        // Zapis danych po każdym poziomie: NazwaZmiennej+indexPoziomu, Wartość
        PlayerPrefs.SetInt("Score" + LevelsData.levelIndex, score);
        PlayerPrefs.SetInt("Highscore" + LevelsData.levelIndex, highscore);
        PlayerPrefs.SetInt("PlayerHP" + LevelsData.levelIndex, player.HP);
        PlayerPrefs.SetInt("CompleteLevel"+LevelsData.levelIndex, 1);
        
        uIManager.completeGamePanel.SetActive(true);
    }
}
