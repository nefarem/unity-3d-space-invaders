using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    // Tablica dwuwymiarowa przechowująca obiekty przeciwników w formie "siatki" lub "macierzowej" dzięki czemu łatwo będzie zaimplementować,
    // który przeciwnik ma strzelać w stronę gracza (bo jest jak by w 1 szeregu),a  także do ruchu w lewo/prawo
    [HideInInspector] public GameObject[,] enemyTab;

    public int currentWave = 1; // Aktualna fala
    public int endWave; // Końcowa fala
    [HideInInspector] public int enemyCounter; // Liczba przeciwników na mapie
    [HideInInspector] public int enemyCountInLine; // Liczba przeciwników w jednym wierszu, wzór: indexPoziomu + numerFali * 3, Każda fala to jedna dodatkowa linia zapełniona przeciwnikami

    [SerializeField] GameObject enemyPrefab; // Prefab przeciwnika do zrespienia
    [SerializeField] GameObject heavyEnemyPrefab; // Prefab ciężkiego przeciwnika który wymaga 2 strzałów a jego zadają 2 dmg
    [SerializeField] GameObject bossPrefab; // Prefab bossa

    GameObject enemyObject;
    GameManager gameManager;

    bool startCoroutine = false;

    void Start()
    {
        gameManager = GetComponent<GameManager>();

        RespawnEnemy();
    }

    void Update()
    {
        if(enemyCounter == 0 && !startCoroutine && currentWave < endWave)
        {
            startCoroutine = true;

            StartCoroutine(SpawnNewWave());
        }

        // Ukończenie poziomów po za 3 który kończy się walką z bossem
        if (enemyCounter == 0 && currentWave == endWave && LevelsData.levelIndex != 3) gameManager.GameOver(true);

        // Zrespienie bossa
        if (enemyCounter == 0 && currentWave == endWave && LevelsData.levelIndex == 3 && !startCoroutine)
        {
            startCoroutine = true;

            Instantiate(bossPrefab, new Vector2(0, 6), Quaternion.identity);
        }
    }

    void RespawnEnemy()
    {
       // Respienie przeciwników:
        /*
            Zwykły przeciwnik z 1 HP respi się przez cały 1 poziom
            Ciężki przeciwnik z 2 HP respi się losowo ze zwykłym przeciwnikiem przez cały 2 poziom
            Ciężki przeciwnik z 2 HP respi się przez cały 3 poziom i na końcu boss z 10 HP

        */

        enemyCountInLine = LevelsData.levelIndex + 3 * 3;

        enemyTab = new GameObject[enemyCountInLine, currentWave];

        for (int x = 0; x < enemyCountInLine; x++)
        {
            for (int y = 0; y <= currentWave - 1; y++)
            {
                switch (LevelsData.levelIndex)
                {
                    case 1:
                        enemyObject = Instantiate(enemyPrefab, new Vector2(-7 + x * 1, 6 + y * 1 - currentWave + 1), Quaternion.identity);
                    break;

                    case 2:

                        //0 - 60 - zwykły przeciwnik
                        //60 - 80 - ciężki przeciwnik

                        int random = Random.Range(0, 81);

                        if(random >= 0 && random <= 60) enemyObject = Instantiate(enemyPrefab, new Vector2(-7 + x * 1, 6 + y * 1 - currentWave + 1), Quaternion.identity);
                        else enemyObject = Instantiate(heavyEnemyPrefab, new Vector2(-7 + x * 1, 6 + y * 1 - currentWave + 1), Quaternion.identity);
                    break;

                    case 3:
                        enemyObject = Instantiate(heavyEnemyPrefab, new Vector2(-7 + x * 1, 6 + y * 1 - currentWave + 1), Quaternion.identity);
                    break;
                }

                enemyObject.GetComponent<Enemy>().positionX = x;
                enemyObject.GetComponent<Enemy>().positionY = y;
                
                // Ustalenie czy przeciwnik ma drop czy nie:
                // Losowanie liczby z przedziału [0, 101)
                //[0 - 80 - brak dropu
                //[81, 101) - ma drop

                int rnd = Random.Range(0, 101);

                switch (rnd)
                {
                    case int n when (n <= 80):
                        enemyObject.GetComponent<EnemyDrop>().hasDrop = false;
                    break;

                    case int n when (n >= 81):
                        enemyObject.GetComponent<EnemyDrop>().hasDrop = true;
                    break;
                }

                enemyTab[x, y] = enemyObject;
                enemyCounter++;
            }
        }
    }

    IEnumerator SpawnNewWave()
    {
        yield return new WaitForSeconds(1.5f);

        currentWave++;

        RespawnEnemy();

        startCoroutine = false;
    }

}
