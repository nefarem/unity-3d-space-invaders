using System;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] Text lifeText;
    [SerializeField] Text scoreText;
    [SerializeField] Text currentWaveText;

    public GameObject gameOverPanel;
    public GameObject nextLevelPanel;
    public GameObject completeGamePanel;

    GameManager gameManager;
    LevelManager levelManager;

    int currentScore;
    int currentHighscore;
    int currentPlayerHP;
    int currentWave;

    void Start()
    {
        gameManager = GetComponent<GameManager>();
        levelManager = GetComponent<LevelManager>();

        currentScore = gameManager.score;
        currentHighscore = gameManager.highscore;
        currentPlayerHP = gameManager.player.HP;
        currentWave = levelManager.currentWave;

        lifeText.text = currentPlayerHP.ToString();
        scoreText.text = "Score: " + currentScore.ToString() + " Highscore: " + currentHighscore.ToString();
        currentWaveText.text = "Fala: " + currentWave.ToString();
    }

    void Update()
    {
        if (currentScore != gameManager.score || currentHighscore != gameManager.highscore || currentPlayerHP != gameManager.player.HP || currentWave != levelManager.currentWave)
        {
            currentScore = gameManager.score;
            currentHighscore = gameManager.highscore;
            currentPlayerHP = gameManager.player.HP; 
            currentWave = levelManager.currentWave;

            lifeText.text = currentPlayerHP.ToString();
            scoreText.text = "Score: " + currentScore.ToString() + " Highscore: " + currentHighscore.ToString(); 
            currentWaveText.text = "Fala: " + currentWave.ToString();
        }
    }
}
