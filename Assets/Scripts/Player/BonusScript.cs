using UnityEngine;

public class BonusScript : MonoBehaviour
{
    public Bonus bonus;

    float speed = 20f;
    SpriteRenderer spriteRenderer;

    void Awake() 
    {
        spriteRenderer = GetComponent<SpriteRenderer>();

        spriteRenderer.sprite = bonus.bonusSprite;
    }

    void Update() 
    {
        Move();

        // Usunięcie gdy wyleci po za mapę
        if (transform.position.y <= -7f) Destroy(gameObject);
    }

    void Move() 
    {
        transform.position = new Vector2(transform.position.x, transform.position.y - 0.1f * speed * Time.deltaTime);
    }
}
