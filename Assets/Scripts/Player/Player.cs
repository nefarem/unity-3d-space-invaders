using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour, IDamage
{
    [SerializeField] Transform[] laserSpawns;
    [SerializeField] GameObject laserPrefab;

    [HideInInspector] public int HP;

    float horizontal; // Pobierania z funkcji GetAxis będzie odpowiadać za przesuniecie gracza w lewo lub prawo gdy naciśnie odpowiedni klawisz [-1, 1]
    float speed = 10f; // Prędkość gracza
    float xPos;

    // Opóźnienie dla strzału
    float timer; 
    float cooldownTime;
    bool canShoot;

    // Bonusy
    [SerializeField] GameObject magicShield; // Magiczna tarcza
    int offensiveBonusAmmo; // Liczba "amunicji" specjalnego ataku umożliwiającego oddanie strzału z 3 działek na raz

    // Dzwieki
    [SerializeField] AudioClip laserShotSFX;
    [SerializeField] AudioClip takeBonusSFX;

    AudioSource audioSource;
    GameManager gameManager;
    CreateVFX createVFX;

    /*
        Zostało do zrobiania bo jest 3 w nocy i mi się chce troszkę spać :D a muszę sporo dokonczyć przed gierką w POE :v
        - Walka z bossem (model bossa + mechaniki)
        - Zapisywanie danych po skończeniu levelu
        - Efekty 
        - Audio
    */
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        createVFX = GameObject.Find("GameManager").GetComponent<CreateVFX>();

        canShoot = true;

        timer = 0.3f;
        cooldownTime = 0.3f;
    }

    void Update()
    {      
        horizontal = Input.GetAxis("Horizontal");

        // Gracz może poruszać się na osi X między wartościami [-7, 7]
        xPos = Mathf.Clamp(transform.position.x, -7, 7) + horizontal * speed * Time.deltaTime;

        // Gdy wartość horizontal jest różna od 0 czyli gracz naciśnie klawisz lewo/prawo wykonana zostanie zmiana pozycji gracza 
        if(horizontal != 0) Move();

        //Gdy gracz naciśnie spację do strzału
        if (Input.GetKeyDown(KeyCode.Space) && canShoot) Fire();

        if(timer <= cooldownTime) 
        {
            timer += Time.deltaTime;
            canShoot = false;
        }
        else canShoot = true;

        if (HP == 0) Kill();
    }

    public void SetDamage(int dmg)
    {
        HP -= dmg;
    }

    void Kill()
    {
        createVFX.SpawnVFX(gameObject.transform.position);

        Destroy(gameObject);
    }

    void Move() 
    {
        transform.position = new Vector2(xPos, transform.position.y);
    }

    void Fire()
    {
        timer = 0f;

        audioSource.clip = laserShotSFX;
        audioSource.Play();

        Instantiate(laserPrefab, laserSpawns[0].position, Quaternion.identity);

        if (offensiveBonusAmmo > 0)
        {
            Instantiate(laserPrefab, laserSpawns[1].position, Quaternion.identity);
            Instantiate(laserPrefab, laserSpawns[2].position, Quaternion.identity);

            offensiveBonusAmmo--;
        } 
    }

    // Gracz otrzymuje bonus
    void SetBonus(BonusScript bonusScript)
    {
        switch (bonusScript.bonus.bonusType)
        {
            case Bonus.BonusEnum.defence:
                magicShield.SetActive(true);
                magicShield.GetComponent<Shield>().hp = 5;
            break;

            case Bonus.BonusEnum.offence:
                offensiveBonusAmmo = 5;
            break;

            case Bonus.BonusEnum.life:
                HP++;
            break;
        }
    }

    void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.gameObject.CompareTag("Bonus"))
        {
            audioSource.clip = takeBonusSFX;
            audioSource.Play();

            BonusScript bonusScript = other.gameObject.GetComponent<BonusScript>();
            SetBonus(bonusScript);

            Destroy(other.gameObject);
        }
    }
}
