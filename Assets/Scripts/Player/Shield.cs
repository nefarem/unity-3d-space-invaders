using UnityEngine;

public class Shield : MonoBehaviour, IKillable, IDamage
{
    public int hp = 5;

    // "Zniszczenie" tarczy która zostanie ukryta i aktywowana gdy gracz znowu podniesie doładowanie
    public void Kill()
    {
        gameObject.SetActive(false);
    }

    public void SetDamage(int dmg)
    {
        hp -= dmg;
    }
}
