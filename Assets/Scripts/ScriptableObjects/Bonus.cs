using UnityEngine;

[CreateAssetMenu(fileName = "New bonus", menuName = "Bonus")]
public class Bonus : ScriptableObject
{
    public Sprite bonusSprite;

    public enum BonusEnum {defence, offence, life}
    public BonusEnum bonusType;
}
