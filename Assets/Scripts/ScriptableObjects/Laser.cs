using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New laser", menuName = "Laser")]
public class Laser : ScriptableObject
{
    // Nazwa 
    public string laserName;

    // Sprite lasera
    public Sprite laserSprite;

    // Czy pocisk ma lecieć w stronę  gracza czy przeciwnika 
    public enum Target {  Player, Enemy }
    public Target laserTarget;

    // Zadawane obrażenia
    public int damage;
}
