using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour
{
    public GameObject menuPanel;
    public GameObject selectLevelPanel;

    public void NewGame()
    {
        SceneManager.LoadScene(1);
    }

    public void OpenSelectLevelMenu()
    {
        menuPanel.SetActive(false);
        selectLevelPanel.SetActive(true);
    }

    public void BackToMenu()
    {
        selectLevelPanel.SetActive(false);
        menuPanel.SetActive(true);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
