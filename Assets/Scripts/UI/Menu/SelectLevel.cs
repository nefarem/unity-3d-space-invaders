using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SelectLevel : MonoBehaviour
{
    [SerializeField] Button[] levelButtonTabs;

    void Start()
    {
        for (int i = 0; i < levelButtonTabs.Length; i++)
        {
            int levelIndex = i + 1;

            if (PlayerPrefs.HasKey("CompleteLevel"+ levelIndex)) levelButtonTabs[i].transform.GetChild(0).GetComponent<Text>().text = "Poziom " + levelIndex + " punkty: " + PlayerPrefs.GetInt("Highscore"+levelIndex) + " życie: " + PlayerPrefs.GetInt("PlayerHP"+levelIndex);
            else if (i > 0 && !PlayerPrefs.HasKey("CompleteLevel"+ levelIndex)) levelButtonTabs[i].interactable = false;
        }
    }

    public void StartLevel(int levelIndex)
    {
        SceneManager.LoadScene(levelIndex);
    }
}
