using UnityEngine;

public class CreateVFX : MonoBehaviour
{
    [SerializeField] GameObject explosionPrefab;
    public void SpawnVFX(Vector2 position)
    {
        Instantiate(explosionPrefab, position, Quaternion.identity);
    }
}
