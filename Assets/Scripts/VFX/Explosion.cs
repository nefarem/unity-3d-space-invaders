using UnityEngine;

public class Explosion : MonoBehaviour
{
    // Zespawnowanie efektu eksplozji po znisdzczeniu przeciwnika lub gracza
    [SerializeField] AudioClip explosionAudio;
    AudioSource audioSource;

    void Start() 
    {
        audioSource = GetComponent<AudioSource>();

        audioSource.clip = explosionAudio;
        audioSource.Play();

        Destroy(gameObject, 1.4f);
    }
}
